import java.util.Scanner;

public class Largest2Ints
{
	private int large; // holds 2nd largest int
	private int largest; // holds largest int
	private Scanner input;
	
	
	public void determineLargest()
	{
		input = new Scanner(System.in);
		int count = 3;//set count equal to 3 so while loop starts on third number
		int current;
		
		System.out.println("This program will take 10 integers from you " + 
		"and find the largest and second largest integer");
		
		System.out.print("Please enter number 1:");
		current = input.nextInt();
		
		largest = current;//initialize 1st number in largest
		
		System.out.print("Please enter number 2:");
		current = input.nextInt();
		
		//initialize large variable with 2nd largest integer
		if (current>=largest)
		{	
			large = largest;
			largest = current;
		}
		else 
		{
			large = current;
		}
		
		//while loop starts with 3rd integer input and sees if larger than either largest or large
		while(count<=10)
		
		{
			System.out.printf("Please enter number %d:", count);
			current = input.nextInt();
					
			if (current > largest )
			{
				if (largest > large)
				{
					large = largest;
				}
				largest = current;
			}
			else if (current > large)
			{
				large = current;
			}
			
			count++;
		} 
		
		
		System.out.printf("The largest number is: %d\nand the second largest is: %d ", largest, large);
	}
}
